`#On créer le network "Lepont" qui servira à lier tous les containers ensemble 

docker network create Lepont

#On créer le container BDD qui sera affecter à un volume (permettant de péréniser les informations) et on le lie au network ">Lepont"

docker run --rm -d --name BDD --network Lepont -v ./data:/var/lib/mysql -e MARIADB_USER=WORDPRESS -e MARIADB_PASSWORD=BDD_PASSWORD -e MARIADB_DATABASE=DATABASE_WORDPRESS -e MARIADB_ROOT_PASSWORD=BDD_PASSWORD  mariadb:11.2.2

#On créer le dossier nginx

mkdir nginx

#On créer le container SCRIPT qui sera affecté à un volume

docker run -d --rm --name SCRIPT --network Lepont -v ./wordpress:/var/www/html php:8.2-apache

#On ajoute l'extension mysqli au container SCRIPT

docker exec SCRIPT bash -c "docker-php-ext-configure mysqli;docker-php-ext-install mysqli;apachectl graceful"

#on créer le fichier par défaut

docker run --rm nginx:1.24.0 cat /etc/nginx/conf.d/default.conf > nginx/default.conf

#on édite le fichier default.conf

vim default.conf

#on ajoute proxy_pass 

server {
    listen       80;
    server_name  HeySamuel;


    location / {
	proxy_pass http://SCRIPT:80;
	proxy_set_header Host $host;
	proxy_set_header X-Real-IP $remote_addr;
    }

}

#On configure nginx avec proxy pass et on le lie au network Lepont

docker run -d --rm --name HTTP --network Lepont -p 80:80 -v ./nginx/default.conf:/etc/nginx/conf.d/default.conf:ro nginx:1.24.0`
